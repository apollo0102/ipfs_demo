/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/ipfs";
exports.ids = ["pages/ipfs"];
exports.modules = {

/***/ "./styles/Ipfs.module.css":
/*!********************************!*\
  !*** ./styles/Ipfs.module.css ***!
  \********************************/
/***/ ((module) => {

eval("// Exports\nmodule.exports = {\n\t\"link\": \"Ipfs_link__h4s_z\"\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdHlsZXMvSXBmcy5tb2R1bGUuY3NzLmpzIiwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYXBpLy4vc3R5bGVzL0lwZnMubW9kdWxlLmNzcz8yNDc5Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIEV4cG9ydHNcbm1vZHVsZS5leHBvcnRzID0ge1xuXHRcImxpbmtcIjogXCJJcGZzX2xpbmtfX2g0c196XCJcbn07XG4iXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./styles/Ipfs.module.css\n");

/***/ }),

/***/ "./pages/ipfs.js":
/*!***********************!*\
  !*** ./pages/ipfs.js ***!
  \***********************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-toastify */ \"react-toastify\");\n/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify/dist/ReactToastify.css */ \"./node_modules/react-toastify/dist/ReactToastify.css\");\n/* harmony import */ var react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify_dist_ReactToastify_css__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react_loader_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-loader-spinner */ \"react-loader-spinner\");\n/* harmony import */ var react_loader_spinner__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_loader_spinner__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _styles_Ipfs_module_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/Ipfs.module.css */ \"./styles/Ipfs.module.css\");\n/* harmony import */ var _styles_Ipfs_module_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_Ipfs_module_css__WEBPACK_IMPORTED_MODULE_6__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_toastify__WEBPACK_IMPORTED_MODULE_3__]);\nreact_toastify__WEBPACK_IMPORTED_MODULE_3__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n\n\n\n\nconst Ipfs = ()=>{\n    const { 0: selectedImage , 1: setSelectedImage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);\n    const [loading, setLoading] = react__WEBPACK_IMPORTED_MODULE_1___default().useState();\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{}, []);\n    const uploadImage = async (event)=>{\n        event.preventDefault();\n        try {\n            setLoading(true);\n            console.log(event.target.files[0]);\n            const file = event.target.files[0];\n            const formData = new FormData();\n            formData.append(\"file\", file);\n            const response = await axios__WEBPACK_IMPORTED_MODULE_2___default().post(\"/api/uploadToAWSHostedNode\", formData, {\n                headers: {\n                    \"Content-Type\": \"multipart/form-data\"\n                }\n            });\n            console.log(\"result\", response, response.data, response.data.URL);\n            setSelectedImage(response.data.URL);\n            react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.success(\"Success!\", {\n                position: react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.POSITION.TOP_RIGHT,\n                autoClose: 2000\n            });\n            setLoading(false);\n        } catch (error) {\n            console.log(\"error:\", error);\n            react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.error(error, {\n                position: react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.POSITION.TOP_RIGHT,\n                autoClose: 2000\n            });\n            setLoading(false);\n        }\n    };\n    return loading ? /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: (_styles_Ipfs_module_css__WEBPACK_IMPORTED_MODULE_6___default().spinnerTag),\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_loader_spinner__WEBPACK_IMPORTED_MODULE_5__.TailSpin, {\n            color: \"#00BFFF\",\n            height: 80,\n            width: 80\n        }, void 0, false, {\n            fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n            lineNumber: 50,\n            columnNumber: 50\n        }, undefined)\n    }, void 0, false, {\n        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n        lineNumber: 50,\n        columnNumber: 15\n    }, undefined) : /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                        children: \"Upload and Display Image using IPFS\"\n                    }, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 53,\n                        columnNumber: 9\n                    }, undefined),\n                    selectedImage && /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                        children: [\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"img\", {\n                                alt: \"not fount\",\n                                width: \"250px\",\n                                src: selectedImage\n                            }, void 0, false, {\n                                fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                                lineNumber: 56,\n                                columnNumber: 13\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n                                fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                                lineNumber: 57,\n                                columnNumber: 13\n                            }, undefined),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n                                onClick: ()=>setSelectedImage(null)\n                                ,\n                                children: \"Remove\"\n                            }, void 0, false, {\n                                fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                                lineNumber: 58,\n                                columnNumber: 13\n                            }, undefined)\n                        ]\n                    }, void 0, true, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 55,\n                        columnNumber: 11\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 61,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 63,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"input\", {\n                        type: \"file\",\n                        name: \"myImage\",\n                        onChange: (event)=>{\n                            uploadImage(event);\n                        }\n                    }, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 64,\n                        columnNumber: 9\n                    }, undefined)\n                ]\n            }, void 0, true, {\n                fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                lineNumber: 52,\n                columnNumber: 7\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                className: (_styles_Ipfs_module_css__WEBPACK_IMPORTED_MODULE_6___default().link),\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                        children: \"Link: \"\n                    }, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 73,\n                        columnNumber: 9\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"span\", {\n                        children: selectedImage\n                    }, void 0, false, {\n                        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                        lineNumber: 74,\n                        columnNumber: 9\n                    }, undefined)\n                ]\n            }, void 0, true, {\n                fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n                lineNumber: 72,\n                columnNumber: 7\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"/root/RUS/Work/AutomatedProp/upload-pinata/pages/ipfs.js\",\n        lineNumber: 51,\n        columnNumber: 5\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Ipfs);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pcGZzLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFtRDtBQUN6QjtBQUNhO0FBQ1E7QUFDQztBQUVEO0FBRS9DLE1BQU1PLElBQUksR0FBRyxJQUFNO0lBQ2pCLE1BQU0sRUFUUixHQVNTQyxhQUFhLEdBVHRCLEdBU3dCQyxnQkFBZ0IsTUFBSVAsK0NBQVEsQ0FBQyxJQUFJLENBQUM7SUFDeEQsTUFBTSxDQUFDUSxPQUFPLEVBQUVDLFVBQVUsQ0FBQyxHQUFHWCxxREFBYyxFQUFFO0lBRTlDQyxnREFBUyxDQUFDLElBQU0sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRXhCLE1BQU1XLFdBQVcsR0FBRyxPQUFPQyxLQUFLLEdBQUs7UUFDbkNBLEtBQUssQ0FBQ0MsY0FBYyxFQUFFLENBQUM7UUFDdkIsSUFBSTtZQUNBSCxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkJJLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDSCxLQUFLLENBQUNJLE1BQU0sQ0FBQ0MsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkMsTUFBTUMsSUFBSSxHQUFHTixLQUFLLENBQUNJLE1BQU0sQ0FBQ0MsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNsQyxNQUFNRSxRQUFRLEdBQUcsSUFBSUMsUUFBUSxFQUFFO1lBQy9CRCxRQUFRLENBQUNFLE1BQU0sQ0FBQyxNQUFNLEVBQUVILElBQUksQ0FBQyxDQUFDO1lBQzlCLE1BQU1JLFFBQVEsR0FBRyxNQUFNcEIsaURBQVUsQ0FDL0IsNEJBQTRCLEVBQzVCaUIsUUFBUSxFQUNSO2dCQUNFSyxPQUFPLEVBQUU7b0JBQ1AsY0FBYyxFQUFFLHFCQUFxQjtpQkFDdEM7YUFDRixDQUNGO1lBQ0RWLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLFFBQVEsRUFBRU8sUUFBUSxFQUFFQSxRQUFRLENBQUNHLElBQUksRUFBRUgsUUFBUSxDQUFDRyxJQUFJLENBQUNDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xFbEIsZ0JBQWdCLENBQUNjLFFBQVEsQ0FBQ0csSUFBSSxDQUFDQyxHQUFHLENBQUMsQ0FBQztZQUNwQ3ZCLHlEQUFhLENBQUMsVUFBVSxFQUFFO2dCQUN4QnlCLFFBQVEsRUFBRXpCLG9FQUF3QjtnQkFDbEM0QixTQUFTLEVBQUUsSUFBSTthQUNoQixDQUFDLENBQUM7WUFDSHJCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuQixDQUFDLE9BQU9zQixLQUFLLEVBQUU7WUFDZGxCLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLFFBQVEsRUFBRWlCLEtBQUssQ0FBQyxDQUFDO1lBQzdCN0IsdURBQVcsQ0FBQzZCLEtBQUssRUFBRTtnQkFDakJKLFFBQVEsRUFBRXpCLG9FQUF3QjtnQkFDbEM0QixTQUFTLEVBQUUsSUFBSTthQUNoQixDQUFDLENBQUM7WUFDSHJCLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuQjtLQUNGO0lBRUQsT0FDRUQsT0FBTyxpQkFBRyw4REFBQ3dCLEtBQUc7UUFBQ0MsU0FBUyxFQUFFN0IsMkVBQWlCO2tCQUFFLDRFQUFDRCwwREFBUTtZQUFDZ0MsS0FBSyxFQUFDLFNBQVM7WUFBQ0MsTUFBTSxFQUFFLEVBQUU7WUFBRUMsS0FBSyxFQUFFLEVBQUU7Ozs7O3FCQUFJOzs7OztpQkFBTSxpQkFDdEcsOERBQUNMLEtBQUc7OzBCQUNGLDhEQUFDQSxLQUFHOztrQ0FDRiw4REFBQ00sSUFBRTtrQ0FBQyxxQ0FBbUM7Ozs7O2lDQUFLO29CQUMzQ2hDLGFBQWEsa0JBQ1osOERBQUMwQixLQUFHOzswQ0FDRiw4REFBQ08sS0FBRztnQ0FBQ0MsR0FBRyxFQUFDLFdBQVc7Z0NBQUNILEtBQUssRUFBRSxPQUFPO2dDQUFFSSxHQUFHLEVBQUVuQyxhQUFhOzs7Ozt5Q0FBSTswQ0FDM0QsOERBQUNvQyxJQUFFOzs7O3lDQUFHOzBDQUNOLDhEQUFDQyxRQUFNO2dDQUFDQyxPQUFPLEVBQUUsSUFBTXJDLGdCQUFnQixDQUFDLElBQUksQ0FBQztnQ0FBQTswQ0FBRSxRQUFNOzs7Ozt5Q0FBUzs7Ozs7O2lDQUMxRDtrQ0FFUiw4REFBQ21DLElBQUU7Ozs7aUNBQUc7a0NBRU4sOERBQUNBLElBQUU7Ozs7aUNBQUc7a0NBQ04sOERBQUNHLE9BQUs7d0JBQ0pDLElBQUksRUFBQyxNQUFNO3dCQUNYQyxJQUFJLEVBQUMsU0FBUzt3QkFDZEMsUUFBUSxFQUFFLENBQUNyQyxLQUFLLEdBQUs7NEJBQ25CRCxXQUFXLENBQUNDLEtBQUssQ0FBQyxDQUFDO3lCQUNwQjs7Ozs7aUNBQ0Q7Ozs7Ozt5QkFDRTswQkFDTiw4REFBQ3FCLEtBQUc7Z0JBQUNDLFNBQVMsRUFBRTdCLHFFQUFXOztrQ0FDekIsOERBQUM4QyxNQUFJO2tDQUFDLFFBQU07Ozs7O2lDQUFPO2tDQUNuQiw4REFBQ0EsTUFBSTtrQ0FBRTVDLGFBQWE7Ozs7O2lDQUFROzs7Ozs7eUJBQ3hCOzs7Ozs7aUJBQ0YsQ0FDTjtDQUNIO0FBQ0QsaUVBQWVELElBQUksRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL2FwaS8uL3BhZ2VzL2lwZnMuanM/ODU5MyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IGF4aW9zIGZyb20gXCJheGlvc1wiO1xuaW1wb3J0IHsgdG9hc3QgfSBmcm9tIFwicmVhY3QtdG9hc3RpZnlcIjtcbmltcG9ydCBcInJlYWN0LXRvYXN0aWZ5L2Rpc3QvUmVhY3RUb2FzdGlmeS5jc3NcIjtcbmltcG9ydCB7IFRhaWxTcGluIH0gZnJvbSBcInJlYWN0LWxvYWRlci1zcGlubmVyXCI7XG5cbmltcG9ydCBzdHlsZXMgZnJvbSBcIi4uL3N0eWxlcy9JcGZzLm1vZHVsZS5jc3NcIjtcblxuY29uc3QgSXBmcyA9ICgpID0+IHtcbiAgY29uc3QgW3NlbGVjdGVkSW1hZ2UsIHNldFNlbGVjdGVkSW1hZ2VdID0gdXNlU3RhdGUobnVsbCk7XG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IFJlYWN0LnVzZVN0YXRlKCk7XG5cbiAgdXNlRWZmZWN0KCgpID0+IHt9LCBbXSk7XG5cbiAgY29uc3QgdXBsb2FkSW1hZ2UgPSBhc3luYyAoZXZlbnQpID0+IHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIHRyeSB7XG4gICAgICAgIHNldExvYWRpbmcodHJ1ZSk7XG4gICAgICBjb25zb2xlLmxvZyhldmVudC50YXJnZXQuZmlsZXNbMF0pO1xuICAgICAgY29uc3QgZmlsZSA9IGV2ZW50LnRhcmdldC5maWxlc1swXTtcbiAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoXCJmaWxlXCIsIGZpbGUpO1xuICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5wb3N0KFxuICAgICAgICBcIi9hcGkvdXBsb2FkVG9BV1NIb3N0ZWROb2RlXCIsXG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgICB7XG4gICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJtdWx0aXBhcnQvZm9ybS1kYXRhXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgfVxuICAgICAgKTtcbiAgICAgIGNvbnNvbGUubG9nKFwicmVzdWx0XCIsIHJlc3BvbnNlLCByZXNwb25zZS5kYXRhLCByZXNwb25zZS5kYXRhLlVSTCk7XG4gICAgICBzZXRTZWxlY3RlZEltYWdlKHJlc3BvbnNlLmRhdGEuVVJMKTtcbiAgICAgIHRvYXN0LnN1Y2Nlc3MoXCJTdWNjZXNzIVwiLCB7XG4gICAgICAgIHBvc2l0aW9uOiB0b2FzdC5QT1NJVElPTi5UT1BfUklHSFQsXG4gICAgICAgIGF1dG9DbG9zZTogMjAwMCxcbiAgICAgIH0pO1xuICAgICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUubG9nKFwiZXJyb3I6XCIsIGVycm9yKTtcbiAgICAgIHRvYXN0LmVycm9yKGVycm9yLCB7XG4gICAgICAgIHBvc2l0aW9uOiB0b2FzdC5QT1NJVElPTi5UT1BfUklHSFQsXG4gICAgICAgIGF1dG9DbG9zZTogMjAwMCxcbiAgICAgIH0pO1xuICAgICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiAoXG4gICAgbG9hZGluZyA/IDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuc3Bpbm5lclRhZ30+PFRhaWxTcGluIGNvbG9yPVwiIzAwQkZGRlwiIGhlaWdodD17ODB9IHdpZHRoPXs4MH0gLz48L2Rpdj4gOlxuICAgIDxkaXY+XG4gICAgICA8ZGl2PlxuICAgICAgICA8aDE+VXBsb2FkIGFuZCBEaXNwbGF5IEltYWdlIHVzaW5nIElQRlM8L2gxPlxuICAgICAgICB7c2VsZWN0ZWRJbWFnZSAmJiAoXG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxpbWcgYWx0PVwibm90IGZvdW50XCIgd2lkdGg9e1wiMjUwcHhcIn0gc3JjPXtzZWxlY3RlZEltYWdlfSAvPlxuICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9eygpID0+IHNldFNlbGVjdGVkSW1hZ2UobnVsbCl9PlJlbW92ZTwvYnV0dG9uPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgICA8YnIgLz5cblxuICAgICAgICA8YnIgLz5cbiAgICAgICAgPGlucHV0XG4gICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgIG5hbWU9XCJteUltYWdlXCJcbiAgICAgICAgICBvbkNoYW5nZT17KGV2ZW50KSA9PiB7XG4gICAgICAgICAgICB1cGxvYWRJbWFnZShldmVudCk7XG4gICAgICAgICAgfX1cbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e3N0eWxlcy5saW5rfT5cbiAgICAgICAgPHNwYW4+TGluazogPC9zcGFuPlxuICAgICAgICA8c3Bhbj57c2VsZWN0ZWRJbWFnZX08L3NwYW4+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbn07XG5leHBvcnQgZGVmYXVsdCBJcGZzO1xuIl0sIm5hbWVzIjpbIlJlYWN0IiwidXNlRWZmZWN0IiwidXNlU3RhdGUiLCJheGlvcyIsInRvYXN0IiwiVGFpbFNwaW4iLCJzdHlsZXMiLCJJcGZzIiwic2VsZWN0ZWRJbWFnZSIsInNldFNlbGVjdGVkSW1hZ2UiLCJsb2FkaW5nIiwic2V0TG9hZGluZyIsInVwbG9hZEltYWdlIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImNvbnNvbGUiLCJsb2ciLCJ0YXJnZXQiLCJmaWxlcyIsImZpbGUiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwicmVzcG9uc2UiLCJwb3N0IiwiaGVhZGVycyIsImRhdGEiLCJVUkwiLCJzdWNjZXNzIiwicG9zaXRpb24iLCJQT1NJVElPTiIsIlRPUF9SSUdIVCIsImF1dG9DbG9zZSIsImVycm9yIiwiZGl2IiwiY2xhc3NOYW1lIiwic3Bpbm5lclRhZyIsImNvbG9yIiwiaGVpZ2h0Iiwid2lkdGgiLCJoMSIsImltZyIsImFsdCIsInNyYyIsImJyIiwiYnV0dG9uIiwib25DbGljayIsImlucHV0IiwidHlwZSIsIm5hbWUiLCJvbkNoYW5nZSIsImxpbmsiLCJzcGFuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/ipfs.js\n");

/***/ }),

/***/ "./node_modules/react-toastify/dist/ReactToastify.css":
/*!************************************************************!*\
  !*** ./node_modules/react-toastify/dist/ReactToastify.css ***!
  \************************************************************/
/***/ (() => {



/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-loader-spinner":
/*!***************************************!*\
  !*** external "react-loader-spinner" ***!
  \***************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-loader-spinner");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = import("react-toastify");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/ipfs.js"));
module.exports = __webpack_exports__;

})();