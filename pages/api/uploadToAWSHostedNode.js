import nextConnect from 'next-connect';
import multer from 'multer';
import { create } from 'ipfs-http-client'
const fs = require('fs');
const fsExtra = require('fs-extra');
const process = require('process');
require('dotenv').config();

async function uploadFileToDecentralizedStorage(fileName) {

    let CID;
    let readableStreamForFile

    console.log("creating Client");
    const client = create("http://135.181.125.159:5001/");
    // const client = create("http://localhost:5001/");

// 
    console.log(client);
    console.log("Client Createedd")
    try {
        readableStreamForFile = fs.createReadStream("uploads/" + fileName);
    }
    catch (error) {
        CID = error;
        console.log(e);
    }

    console.log(".................................................................")
    console.log("readableStreamForFile", readableStreamForFile);
    console.log(".................................................................")
    console.log("Reading the file from the server is done, Now uploading the file to AWS hosted IPFS Node");

    try {
        CID = await client.add(readableStreamForFile)

    } catch (error) {
        console.log(error);
        CID = error;
    }


    console.log(".................................................................")
    console.log("Only CID(CID.cid)", CID.cid);
    console.log(".................................................................")
    console.log("String (CID.cid)", CID.cid.toString())
    console.log(".................................................................")

    const pinning = await client.pin.add(CID.cid.toString());
    console.log(".................................................................")
    console.log("File Obj", CID);
    console.log(".................................................................")
    console.log("Pinning: ", pinning);
    console.log(".................................................................")

    const result = CID.cid;

    console.log("Result:", result);

    return CID;

}

const upload = multer({
    storage: multer.diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => cb(null, file.originalname),
    }),
});

// console.log(upload);

const apiRoute = nextConnect({
    onError(error, req, res) {
        res.status(501).json({ error: `Sorry something Happened! ${error.message}` });
    },
    onNoMatch(req, res) {
        res.status(405).json({ error: `Method '${req.method}' Not Allowed` });
    },

});

apiRoute.use(upload.single('file'));

apiRoute.post(async (req, res) => {
    try {
        console.log("Uploading: " + req.file.originalname);
        let cid = await uploadFileToDecentralizedStorage(req.file.originalname);
        console.log("Result At Multer: " + JSON.stringify(cid));
        if (cid) {
            fsExtra.emptyDirSync("./uploads/")
        }
        res.status(200).json({
            IpfsHash: cid.path,
            //URL: "http://3.36.219.75:8080/ipfs/" + cid.path,
            URL: "https://crustwebsites.net/ipfs/" + cid.path,
        });
    } catch {
        res.status(501).json({ error: 'Error occurred while uploading'})
    }
    

    // res.status(200).json({
    //     result:"mmmmmmm"
    // });
});

export default apiRoute;

export const config = {
    api: {
        bodyParser: false, // Disallow body parsing, consume as stream
    },
};