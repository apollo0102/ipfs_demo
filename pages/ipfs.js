import React, { useEffect, useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { TailSpin } from "react-loader-spinner";

import styles from "../styles/Ipfs.module.css";

const Ipfs = () => {
  const [selectedImage, setSelectedImage] = useState(null);
  const [loading, setLoading] = React.useState();

  useEffect(() => {}, []);

  const uploadImage = async (event) => {
    event.preventDefault();
    try {
        setLoading(true);
      console.log(event.target.files[0]);
      const file = event.target.files[0];
      const formData = new FormData();
      formData.append("file", file);
      const response = await axios.post(
        "/api/uploadToAWSHostedNode",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log("result", response, response.data, response.data.URL);
      setSelectedImage(response.data.URL);
      toast.success("Success!", {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 2000,
      });
      setLoading(false);
    } catch (error) {
      console.log("error:", error);
      toast.error(error, {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 2000,
      });
      setLoading(false);
    }
  };

  return (
    loading ? <div className={styles.spinnerTag}><TailSpin color="#00BFFF" height={80} width={80} /></div> :
    <div>
      <div>
        <h1>Upload and Display Image using IPFS</h1>
        {selectedImage && (
          <div>
            <img alt="not fount" width={"250px"} src={selectedImage} />
            <br />
            <button onClick={() => setSelectedImage(null)}>Remove</button>
          </div>
        )}
        <br />

        <br />
        <input
          type="file"
          name="myImage"
          onChange={(event) => {
            uploadImage(event);
          }}
        />
      </div>
      <div className={styles.link}>
        <span>Link: </span>
        <span>{selectedImage}</span>
      </div>
    </div>
  );
};
export default Ipfs;
